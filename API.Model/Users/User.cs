﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;

namespace API.Model.Users
{

    public class User : BaseModel
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string UserId { get; set; }

        public string Firstname { get; set; }

        public string Lastname { get; set; }

        public string Secret { get; set; }

        public DateTime DateOfBirth { get; set; }
        
        public string ContactNumber { get; set; }

        public string EmailAddress { get; set; }

        public DateTime DateOfEmployment { get; set; }
    }
}
