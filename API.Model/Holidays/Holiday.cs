﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API.Model.Holidays
{
    public class Holiday : BaseModel
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public int HolidayId { get; set; }

        public string Description { get; set; }

        public DateTime DateOfHoliday { get; set; }

    }
}
