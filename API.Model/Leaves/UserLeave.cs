﻿using API.Model.Users;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;

namespace API.Model.Leaves
{
    public class UserLeave : BaseModel
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public int UserLeaveId { get; set; }

        public User User { get; set; }

        public LeaveType LeaveType { get; set; }

        public double AmountLeft { get; set; }

        public double AmountTaken { get; set; }

        public DateTime StartDate { get; set; }

        public DateTime EndDate { get; set; }

        public bool IsAccepted { get; set; }

        public bool IsPending { get; set; }

        public DateTime LeaveExpireDate { get; set; }
    }
}
