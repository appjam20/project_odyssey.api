﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace API.Model.Leaves
{
    public class LeaveType : BaseModel
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public int LeaveTypeId { get; set; }

        public string Description { get; set; }

        public bool IsUnpaid { get; set; }

        public bool IsAdmin { get; set; }
    }
}


