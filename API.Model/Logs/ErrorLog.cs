﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;

namespace API.Model.Logs
{
    public class ErrorLog : BaseModel
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public int ErrorLogId { get; set; }

        public string Level { get; set; }

        public string Method { get; set; }

        public List<string> Parameters { get; set; }

        public string ErrorMessage { get; set; }

        public string StackTrace { get; set; }
    }
}
