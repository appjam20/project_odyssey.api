﻿using API.Model.Leaves;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API.DAL.Leaves
{
    public interface ILeaveRepository
    {
        #region LeaveTypes

        public List<LeaveType> GetAllActiveLeaveTypes();

        public List<LeaveType> GetAdminOnlyLeave();

        public LeaveType GetSpecificLeave(LeaveType leaveType);

        public bool AddLeaveType(LeaveType newLeaveType);

        public bool DeleteLeaveType(LeaveType leaveType);

        #endregion

        #region UserLeave

        public List<UserLeave> GetUserLeaveByUser(string userId);

        public UserLeave GetUserLeaveSpecific(int userLeaveId);

        public List<UserLeave> GetPendingLeaveByUser(string userId);

        public bool SaveUserLeave(UserLeave newUserLeave);

        public bool UpdateUserLeave(UserLeave userLeave);

        public bool DeleteUserLeave(UserLeave userLeave);

        #endregion
    }
}
