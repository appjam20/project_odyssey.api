﻿using API.Model.Leaves;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API.DAL.Leaves
{
    public class LeaveRepository: ILeaveRepository
    {
        private readonly OdysseyContext _core;

        public LeaveRepository()
        {
            _core = new OdysseyContext();
        }

        #region LeaveTypes

        public List<LeaveType> GetAllActiveLeaveTypes()
        {
            return _core.LeaveTypes.Where(x => x.IsActive && !x.IsAdmin).ToList();
        }

        public List<LeaveType> GetAdminOnlyLeave()
        {
            return _core.LeaveTypes.Where(x => x.IsAdmin && x.IsActive).ToList();
        }

        public LeaveType GetSpecificLeave(LeaveType leaveType)
        {
            return _core.LeaveTypes.FirstOrDefault(x => x.Description == leaveType.Description && x.IsActive);
        }

        public bool AddLeaveType(LeaveType newLeaveType)
        {
            using (var context = _core)
            {
                _core.LeaveTypes.Add(newLeaveType);
                return (_core.SaveChanges()) > 0;
            }
        }

        public bool DeleteLeaveType(LeaveType leaveType)
        {
            var getLeaveType = GetSpecificLeave(leaveType);

            getLeaveType.IsActive = false;

            return (_core.SaveChanges()) > 0;
        }

        #endregion

        #region UserLeave

        public List<UserLeave> GetUserLeaveByUser(string userId)
        {
            return _core.UserLeaves.Where(x => x.IsActive && x.User.UserId == userId).ToList();
        }

        public UserLeave GetUserLeaveSpecific(int userLeaveId)
        {
            return _core.UserLeaves.FirstOrDefault(x => x.UserLeaveId == userLeaveId);
        }

        public List<UserLeave> GetPendingLeaveByUser(string userId)
        {
            return _core.UserLeaves.Where(x => x.IsActive && x.IsPending && x.User.UserId == userId).ToList();
        }

        public bool SaveUserLeave(UserLeave newUserLeave)
        {
            _core.UserLeaves.Add(newUserLeave);
            return (_core.SaveChanges()) > 0;
        }

        public bool UpdateUserLeave(UserLeave userLeave)
        {
            var getCurrentUserLeave = GetUserLeaveSpecific(userLeave.UserLeaveId);

            _core.Entry(getCurrentUserLeave).CurrentValues.SetValues(userLeave);
            return (_core.SaveChanges()) > 0;
        }

        public bool DeleteUserLeave(UserLeave userLeave)
        {
            var getCurrentUserLeave = GetUserLeaveSpecific(userLeave.UserLeaveId);

            getCurrentUserLeave.IsActive = false;

            return (_core.SaveChanges()) > 0;
        }

        #endregion
    }
}
