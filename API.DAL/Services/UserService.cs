﻿using API.Model.Users;
using API.Context
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;

namespace API.DAL.Services
{
    public class UserService
    {
        private readonly IMongoCollection<User> _employees;
        public UserService(Contexts.IUserDatabasesetting settings)
        {
            var client = new MongoClient(settings.ConnectionString);
            var database = client.GetDatabase(settings.DatabaseName);

            _employees = database.GetCollection<User>(settings.UsersCollectionName);

        }

        public List<User> Get()
        {
            List<User> employees;
            employees = _employees.Find(emp => true).ToList();
            return employees;
        }

        public User Get(string userId) =>
            _employees.Find<User>(usr => usr.UserId == userId).FirstOrDefault();

    }
}
