﻿using API.Model.Logs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API.DAL.ErrorLogs
{
    interface IErrorLogRepository
    {
        public bool InsertErrorLog(ErrorLog errorLog);
    }
}
