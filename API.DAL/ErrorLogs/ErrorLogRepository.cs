﻿using API.Model.Logs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API.DAL.ErrorLogs
{
    public class ErrorLogRepository: IErrorLogRepository
    {
        private readonly OdysseyContext _core;

        public ErrorLogRepository()
        {
            _core = new OdysseyContext();
        }

        public bool InsertErrorLog(ErrorLog errorLog)
        {
            _core.ErrorLogs.Add(errorLog);
            return (_core.SaveChanges()) > 0;
        }
    }
}
