﻿using API.Model.Holidays;
using API.Model.Leaves;
using API.Model.Logs;
using API.Model.Users;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System.Configuration;

namespace API.DAL
{
    public class OdysseyContext : IdentityDbContext
    {
        static readonly string connectionString = ConfigurationManager.ConnectionStrings["CoreContext"].ConnectionString;

        public OdysseyContext()
        {
        }

        public OdysseyContext(DbContextOptions<OdysseyContext> options) : base(options)
        {
        }


        public DbSet<User> Users { get; set; }
        public DbSet<LeaveType> LeaveTypes { get; set; }
        public DbSet<UserLeave> UserLeaves { get; set; }
        public DbSet<Holiday> Holidays { get; set; }
        public DbSet<ErrorLog> ErrorLogs { get; set; }

    }
}
