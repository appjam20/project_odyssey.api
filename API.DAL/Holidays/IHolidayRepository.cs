﻿using API.Model.Holidays;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API.DAL.Holidays
{
    public interface IHolidayRepository
    {
        public List<Holiday> GetHolidays();
    }
}
