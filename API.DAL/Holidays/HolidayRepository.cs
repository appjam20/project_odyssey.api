﻿using API.Model.Holidays;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API.DAL.Holidays
{
    public class HolidayRepository : IHolidayRepository
    {
        private readonly OdysseyContext _core;
        public HolidayRepository()
        {
            _core = new OdysseyContext();
        }

        public List<Holiday> GetHolidays()
        {
            return _core.Holidays.Where(x => x.IsActive).ToList();
        }
    }
}
