﻿using API.Model.Users;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API.DAL.Users
{
    public class UserRepository: IUserRepository
    {
        private readonly OdysseyContext _core;

        public UserRepository()
        {
            _core = new OdysseyContext();
        }

        public User CheckUserExcist(string emailAddress)
        {
            using (var context = _core)
            {
                var user = context.Users.FirstOrDefault(x => x.EmailAddress == emailAddress && x.IsActive);

                return user;
            }
        }

        public User RegisterUser(User addUser)
        {
            using (var context = _core)
            {
                context.Users.Add(addUser);

                return context.Users.FirstOrDefault(x => x.EmailAddress == addUser.EmailAddress && x.IsActive);
            }
        }

        public User VerifyUser(User user)
        {
            using (var context = _core)
            {
                return context.Users.FirstOrDefault(x => x.EmailAddress == user.EmailAddress && user.Secret == user.Secret && x.IsActive);
            }
        }

        public User UpdateUser(User user)
        {
            using (var context = _core)
            {
                var getCurrentUser = VerifyUser(user);

                _core.Entry(getCurrentUser).CurrentValues.SetValues(user);
                _core.SaveChanges();

                return VerifyUser(user);
            }
        }

        public bool DeleteUser(User user)
        {
            var getCurrentUser = VerifyUser(user);

            using (var context = _core)
            {
                getCurrentUser.IsActive = false;

                return (_core.SaveChanges())>0;
            }
        }
    }
}
