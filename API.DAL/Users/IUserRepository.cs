﻿using API.Model.Users;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API.DAL.Users
{
    public interface IUserRepository
    {
        public User CheckUserExcist(string emailAddress);
        public User RegisterUser(User addUser);
        public User VerifyUser(User user);
        public User UpdateUser(User user);
        public bool DeleteUser(User user);
    }
}
