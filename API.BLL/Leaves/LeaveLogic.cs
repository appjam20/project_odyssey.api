﻿using API.DAL.Holidays;
using API.DAL.Leaves;
using API.Model.Holidays;
using API.Model.Leaves;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API.BLL.Leaves
{
    public class LeaveLogic : ILeaveLogic
    {

        private ILeaveRepository _leaveRepository = new LeaveRepository();
        private IHolidayRepository _holidayRepository = new HolidayRepository();

        #region LeaveType

        public List<LeaveType> GetAllActiveLeaveTypes()
        {
            return _leaveRepository.GetAllActiveLeaveTypes();
        }

        public List<LeaveType> GetAdminOnlyLeave()
        {
            return _leaveRepository.GetAdminOnlyLeave();
        }

        public LeaveType GetSpecificLeave(LeaveType leaveType)
        {
            return _leaveRepository.GetSpecificLeave(leaveType);
        }

        public bool AddLeaveType(LeaveType newLeaveType)
        {
            bool result = false;

            if (newLeaveType != null)
            {
                result = _leaveRepository.AddLeaveType(newLeaveType);
            }

            return result;
        }

        public bool DeleteLeaveType(LeaveType leaveType)
        {
            bool result = false;

            if (leaveType != null)
            {
                result = _leaveRepository.DeleteLeaveType(leaveType);
            }

            return result;
        }
        #endregion

        #region Holidays

        public List<Holiday> GetHolidays()
        {
            return _holidayRepository.GetHolidays();
        }

        #endregion

        #region UserLeave

        public List<UserLeave> GetUserLeaveByUser(string userId)
        {
            return _leaveRepository.GetUserLeaveByUser(userId);
        }

        public UserLeave GetUserLeaveSpecific(int userLeaveId)
        {
            return _leaveRepository.GetUserLeaveSpecific(userLeaveId);
        }

        public List<UserLeave> GetPendingLeaveByUser(string userId)
        {
            return _leaveRepository.GetPendingLeaveByUser(userId);
        }

        public bool SaveUserLeave(UserLeave newUserLeave)
        {
            bool result = false;
            if (newUserLeave != null)
            {
                result =  _leaveRepository.SaveUserLeave(CalculateUserLeave(newUserLeave));
            }

            return result;
        }

        public bool UpdateUserLeave(UserLeave userLeave)
        {
            bool result = false;
            if (userLeave != null)
            {
                result = _leaveRepository.UpdateUserLeave(userLeave);
            }

            return result;
        }

        public bool DeleteUserLeave(UserLeave userLeave)
        {
            bool result = false;
            if (userLeave != null)
            {
                result = _leaveRepository.DeleteUserLeave(userLeave);
            }

            return result;
        }

        #endregion

        #region UserLeaveLogic

        public UserLeave CalculateUserLeave(UserLeave userLeave)
        {
            var newUserLeave = new UserLeave();
            var getHolidayList = GetHolidays();
            //var getUserLeave = GetUserLeaveByUser(userLeave.User.UserId).Where(x => x.LeaveType == userLeave.LeaveType && x.IsAccepted).
            //                   OrderByDescending(x=>x.EndDate).
            //                   Select(x=>x.AmountLeft);

            //var totalDays = (userLeave.EndDate - userLeave.StartDate).TotalDays;

            return null;
        }

        #endregion

    }
}
