﻿using API.DAL.Users;
using API.Model.Users;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API.BLL.Users
{
    public class UserLogic: IUserLogic
    {
        private IUserRepository _userRepository = new UserRepository();

        public User GetExcistingUser(User user)
        {
            return _userRepository.VerifyUser(user);
        }

        public bool CheckIfUserExists(string emailAddress)
        {
            bool result = false;
            var CheckForUser = _userRepository.CheckUserExcist(emailAddress);

            if (CheckForUser.UserId != null)
            {
                result = true;
            }

            return result;
        }

        public User CreateUser(User user)
        {
            var createdUser = _userRepository.RegisterUser(user);

            if (createdUser.UserId != null)
                return createdUser;

            return null;
            //Write to errorLog
        }

        public User UpdateUserProfile(User user)
        {
            if (user.UserId != null)
                return _userRepository.UpdateUser(user);

            return null;
            //Write to errorlog
        }

        public bool DeleteUserProfile(User user)
        {
            if (user.UserId != null)
                return _userRepository.DeleteUser(user);

            return false;
            //Write to errorlog
        }
    }
}
