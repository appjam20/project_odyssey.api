﻿using API.Model.Users;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API.BLL.Users
{
    public interface IUserLogic
    {
        public bool CheckIfUserExists(string emailAddress);
        public User CreateUser(User user);
        public User GetExcistingUser(User user);
        public User UpdateUserProfile(User user);
        public bool DeleteUserProfile(User user);
    }
}
