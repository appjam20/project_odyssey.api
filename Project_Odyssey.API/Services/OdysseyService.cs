﻿using MongoDB.Driver;
using Project_Odyssey.API.Contexts;
using Project_Odyssey.API.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Project_Odyssey.API.Services
{
    public class OdysseyService
    {
        #region Properties
        private readonly IMongoCollection<User> _user;
        private readonly IMongoCollection<LeaveType> _leaveType;
        private readonly IMongoCollection<UserLeave> _userLeave;
        private readonly IMongoCollection<Holiday> _holiday;
        #endregion

        #region Constructor
        public OdysseyService(IOdysseyDatabasesettings settings)
        {
            var client = new MongoClient(settings.ConnectionString);
            var database = client.GetDatabase(settings.DatabaseName);

            _user = database.GetCollection<User>(settings.UsersCollectionName);
            _leaveType = database.GetCollection<LeaveType>(settings.LeaveTypeCollectionName);
            _userLeave = database.GetCollection<UserLeave>(settings.UserLeaveCollectionName);
            _holiday = database.GetCollection<Holiday>(settings.HolidayCollectionName);
        }
        #endregion

        #region Holidays

        public List<Holiday> GetAllHolidays()
        {
            return _holiday.Find(x => x.IsActive).ToList();
        }

        #endregion

        #region Leave Types
        
        public List<LeaveType> GetAllLeaveTypes()
        {
            return _leaveType.Find(x => x.IsActive).ToList();
        }

        public LeaveType GetSpecificLeaveType(string leaveId)
        {
            return _leaveType.Find(x => x.Id == leaveId && x.IsActive).FirstOrDefault();
        }

        #endregion

        #region Users

        public bool CheckForExistingUser(string emailAddress)
        {
            bool result = false;

            var existingUser = _user.Find(x => x.IsActive && x.EmailAddress == emailAddress.ToLower()).FirstOrDefault();

            if (existingUser != null)
                result = true;

            return result;
        }

        public User GetUser(User user)
        {
            return _user.Find(x => x.IsActive && x.EmailAddress == user.EmailAddress.ToLower() && x.Secret == user.Secret).FirstOrDefault();
        }

        public User CreateNewUser(User user)
        {
            _user.InsertOne(user);
            return GetUser(user);
        }

        public User UpdateUser(User user)
        {
            var userFilter = Builders<User>.Filter.Eq("Id", user.Id);

            _user.FindOneAndReplace(userFilter, user);
            return GetUser(user);
        }

        public bool DeleteUser(User user)
        {
            var userFilter = Builders<User>.Filter.Eq("Id", user.Id);

            _user.FindOneAndDelete(userFilter);

            var currentUser = GetUser(user);

            if (currentUser != null)
            {
                return false;
            }

            return true;
        }

        #endregion

        #region User Leaves

        public List<UserLeave> GetAllLeaveForUser(string userEmailAdress)
        {
            return _userLeave.Find(x=>x.User_EmailAddress == userEmailAdress && x.IsActive).ToList();
        }

        public UserLeave GetSpecificUserLeave(string userLeaveId)
        {
            return _userLeave.Find(x => x.Id == userLeaveId).FirstOrDefault();
        }

        public UserLeave GetLatestUserLeaveForUser(UserLeave userLeave)
        {
            return _userLeave.AsQueryable()
                             .Where(x => x.User_Id == userLeave.User_Id && x.LeaveType_Id == userLeave.LeaveType_Id && x.IsAccepted && x.IsActive)
                             .OrderByDescending(x=>x.ModifiedDate)
                             .FirstOrDefault();
        }

        public bool CreateNewUserLeave(UserLeave userLeave)
        {
            bool result = false;
            _userLeave.InsertOne(userLeave);
            var getNewUserLeave = GetSpecificUserLeave(userLeave.Id);

            if (getNewUserLeave != null)
                result = true;

            return result;
        }

        public UserLeave UpdateUserLeave(UserLeave userLeave)
        {
            var userLeaveFilter = Builders<UserLeave>.Filter.Eq("Id", userLeave.Id);

            _userLeave.FindOneAndReplace(userLeaveFilter, userLeave);

            return GetSpecificUserLeave(userLeave.Id);
        }

        #endregion
    }
}
