﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Project_Odyssey.API.Logic;
using Project_Odyssey.API.Models;
using Project_Odyssey.API.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Project_Odyssey.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LeaveTypeController : Controller
    {
        private readonly LeaveTypeLogic _leaveTypeLogic;

        public LeaveTypeController(LeaveTypeLogic leaveTypeLogic)
        {
            _leaveTypeLogic = leaveTypeLogic;
        }

        [Route("GetAllLeaveTypes")]
        [HttpGet]
        public List<LeaveType> GetAllLeaveTypes()
        {
            return _leaveTypeLogic.GetAllLeaveTypes();
        }

        [Route("GetSpecificLeaveType")]
        [HttpGet]
        public LeaveType GetSpecificLeaveType(string leaveTypeId)
        {
            return _leaveTypeLogic.GetSpecificLeaveType(leaveTypeId);
        }
    }
}
