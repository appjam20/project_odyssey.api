﻿using Microsoft.AspNetCore.Mvc;
using Project_Odyssey.API.Logic;
using Project_Odyssey.API.Models;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Project_Odyssey.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        //private IUserLogic _userLogic = new UserLogic();

        private readonly UserLogic _userLogic;

        public UserController(UserLogic userService)
        {
            _userLogic = userService;
        }

        [Route("CheckUser")]
        [HttpGet]
        public bool CheckUser(string emailAddress)
        {
            return _userLogic.CheckForExistingUser(emailAddress);
        }

        [Route("GetUser")]
        [HttpGet]
        public User GetUser(User user)
        {
            return _userLogic.GetUser(user);
        }

        [Route("CreateUser")]
        [HttpPost]
        public User CreateUser(User user)
        {
            return _userLogic.CreateNewUser(user);
        }

        [Route("UpdateUser")]
        [HttpPut]
        public User UpdateUser(User user)
        {
            return _userLogic.UpdateUser(user);
        }

        [Route("DeleteUser")]
        [HttpDelete]
        public bool DeleteUser(User user)
        {
            return _userLogic.DeleteUser(user);
        }
    }
}
