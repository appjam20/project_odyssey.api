﻿using Microsoft.AspNetCore.Mvc;
using Project_Odyssey.API.Logic;
using Project_Odyssey.API.Models;
using Project_Odyssey.API.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Project_Odyssey.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserLeaveController : Controller
    {
        private readonly UserLeaveLogic _userLeaveLogic;

        public UserLeaveController(UserLeaveLogic userLeaveLogic)
        {
            _userLeaveLogic = userLeaveLogic;
        }

        [Route("GetAllLeaveForUser")]
        [HttpGet]
        public List<UserLeave> GetAllLeaveForUser(string userEmailAdress)
        {
            return _userLeaveLogic.GetAllLeaveForUser(userEmailAdress);
        }

        [Route("UpdateUserLeave")]
        [HttpPut]
        public UserLeave UpdateUserLeave(UserLeave userLeave)
        {
            return _userLeaveLogic.UpdateUserLeave(userLeave);
        }

        [Route("CreateNewUserLeave")]
        [HttpPost]
        public bool CreateNewUserLeave(UserLeave userLeave)
        {
            return _userLeaveLogic.CreateNewUserLeave(userLeave);
        }

        [Route("GetSpecificUserLeave")]
        [HttpGet]
        public UserLeave GetSpecificUserLeave(string userLeaveId)
        {
            return _userLeaveLogic.GetSpecificUserLeave(userLeaveId);
        }
    }
}
