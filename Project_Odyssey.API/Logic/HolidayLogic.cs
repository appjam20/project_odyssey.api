﻿using Project_Odyssey.API.Models;
using Project_Odyssey.API.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Project_Odyssey.API.Logic
{
    public class HolidayLogic
    {
        private readonly OdysseyService _odysseyService;

        public HolidayLogic(OdysseyService odysseyService)
        {
            _odysseyService = odysseyService;
        }

        public List<Holiday> GetHolidays()
        {
            return _odysseyService.GetAllHolidays();
        }
    }
}
