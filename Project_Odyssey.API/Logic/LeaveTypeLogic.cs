﻿using Project_Odyssey.API.Models;
using Project_Odyssey.API.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Project_Odyssey.API.Logic
{
    public class LeaveTypeLogic
    {
        private readonly OdysseyService _odysseyService;

        public LeaveTypeLogic(OdysseyService odysseyService)
        {
            _odysseyService = odysseyService;
        }

        public List<LeaveType> GetAllLeaveTypes()
        {
            return _odysseyService.GetAllLeaveTypes();
        }

        public LeaveType GetSpecificLeaveType(string leaveId)
        {
            return _odysseyService.GetSpecificLeaveType(leaveId);
        }
    }
}
