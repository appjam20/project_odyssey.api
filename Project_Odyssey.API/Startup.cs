using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using Microsoft.Extensions.Options;
using Project_Odyssey.API.Contexts;
using Project_Odyssey.API.Services;
using Project_Odyssey.API.Logic;

namespace Project_Odyssey.API
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<OdysseyDatabasesettings>(
                Configuration.GetSection(nameof(OdysseyDatabasesettings)));

            services.AddSingleton<IOdysseyDatabasesettings>(sp =>
                sp.GetRequiredService<IOptions<OdysseyDatabasesettings>>().Value);

            services.AddSingleton<OdysseyService>();

            services.AddSingleton<HolidayLogic>();
            services.AddSingleton<LeaveTypeLogic>();
            services.AddSingleton<UserLeaveLogic>();
            services.AddSingleton<UserLogic>();

            services.AddControllers();
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "Project_Odyssey.API", Version = "v1" });
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
