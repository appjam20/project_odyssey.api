﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;

namespace Project_Odyssey.API.Models
{
    public class LeaveType
    {

        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }

        public string Description { get; set; }

        public bool IsActive { get; set; }

        public bool IsUnpaid { get; set; }

        public bool IsAdmin { get; set; }

        public bool IsSpecial { get; set; }

        public bool IsWorkingDayOnly { get; set; }

        public string CreatedBy { get; set; }

        public string ModifiedBy { get; set; }

        public DateTime CreatedDate { get; set; }

        public DateTime ModifiedDate { get; set; }
    }
}
