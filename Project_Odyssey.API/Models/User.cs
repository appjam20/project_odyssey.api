﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;

namespace Project_Odyssey.API.Models
{
    public class User
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }

        public string Firstname { get; set; }

        public string Lastname { get; set; }

        public string Secret { get; set; }

        public DateTime DateOfBirth { get; set; }

        public string Nickname { get; set; }

        public string Gender { get; set; }

        public string ContactNumber { get; set; }

        public string EmailAddress { get; set; }

        public DateTime DateOfEmployment { get; set; }

        public bool IsActive { get; set; }

        public string CreatedBy { get; set; }

        public string ModifiedBy { get; set; }

        public DateTime CreatedDate { get; set; }

        public DateTime ModifiedDate { get; set; }
    }
}
